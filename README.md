# Ruby on Rails Tutorial sample application
This is the sample application for
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](https://www.railstutorial.org/)
(6th Edition)

## Getting started
To get started with the app, clone the repo and then install the needed gems:
```
$ bundle config without production
$ bundle install
$ bundle update
```
Next, migrate the database:
```
$ rails db:migrate
```
Finally, run the test suite to verify that everything is working correctly:
```
$ rails test
```
If the test suite passes, you'll be ready to run the app in a local server:
```
$ rails server
```

## Chapter 4
```
To start with a new feature branch:
$ git flow feature start rails-flavored-ruby
```
### 4.1
 Built in helpers: (DRY Principle implementation)
- application_helper.rb (app/helpers/application_helper.rb)
- Returns the full title on a per-page basis.
def full_title(page_title = '')
    base_title = "Ruby on Rails Tutorial Sample App"
        if page_title.empty?
            base_title
        else
            page_title + " | " + base_title
    end
end

Replace home page from:
<title><%= yield(:title) %> | Ruby on Rails Tutorial Sample App</title>
With:
<title><%= full_title(yield(:title)) %></title>

Which basically removes the redanduncy of "| Ruby on Rails Tutorial Sample App" on the application home page.
Test cases, success. (test/controllers/static_pages_controller_test.rb)

### 4.2 
1. Assign variables city and state to your current city and state of residence. (If residing outside the U.S., substitute the analogous quantities.)
>> city = "Gerona"
>> state = "Tarlac"
2. Using interpolation, print (using puts) a string consisting of the city and state separated by a comma and a space, as in “Los Angeles, CA”.
>> puts "#{city}, #{state}"
3. Repeat the previous exercise but with the city and state separated by a tab character.
>> puts "#{city}#{state}"
4. What is the result if you replace double quotes with single quotes in the previous exercise?
=> #{city}#{state}
5. What is the length of the string “racecar”?
>> s = "racecar"
>> s.length
6. Confirm using the reverse method that the string in the previous exercise is the same when its letters are reversed.
>> s.reverse
=> "racecar"
7. Assign the string “racecar” to the variable s. Confirm using the comparison operator == that s and s.reverse are equal.
>> s == s.reverse
=> true
8. What is the result of running the code shown in Listing 4.9? How does it change if you reassign the variable s to the string “onomatopoeia”?
=> nil
9. By replacing FILL_IN with the appropriate comparison test shown in Listing 4.10, define a method for testing palindromes. Hint: Use the comparison shown in Listing 4.9.
>> if s == s.reverse
10. By running your palindrome tester on “racecar” and “onomatopoeia”, confirm that the first is a palindrome and the second isn’t.
It's not a palindrome!
=> nil
11. By calling the nil? method on palindrome_tester("racecar"), confirm that its return value is nil (i.e., calling nil? on the result of the
method should return true). This is because the code in Listing 4.10 prints its responses instead of returning them.
=> true
### 4.3
1. Assign a to be to the result of splitting the string “A man, a plan, a canal, Panama” on comma-space.
>>  a = ("A man, a plan, a canal, Panama").split(", ")
=> ["A man", "a plan", "a canal", "Panama"]
2. Assign s to the string resulting from joining a on nothing.
>> s = a.join
=> "A mana plana canalPanama"
3.  Split s on whitespace and rejoin on nothing. Use the palindrome test from Listing 4.10 to confirm that the resulting string s is not a palindrome by the current definition. Using the downcase method, show that s.downcase is a palindrome.
>> s = s.split(' ').join.downcase
>> s == s.reverse
=> true
4. What is the result of selecting element 7 from the range of letters a through z? What about the same range reversed?
>> ('a'..'z').to_a[7]
=> "h"
>> ('a'..'z').to_a.reverse[7]
=> "s"
5. Using the range 0..16, print out the first 17 powers of 2.
>> (0..16).each {|i| puts 2**i}
6. Define a method called yeller that takes in an array of characters and returns a string with an ALLCAPS version of the input. Verify that yeller(['o', 'l', 'd']) returns "OLD". Hint: Combine map, upcase, and join.
?> def yeller(s)
?>   s = s.map {|i| i. upcase}
?>   s.join
>> end
=> "OLD"
7. Define a method called random_subdomain that returns a randomly generated string of eight letters.
>> def random_subdomain(a)
?>   a = ('a'..'z').to_a.shuffle[0..7].join
>> end
8. By replacing the question marks in Listing 4.12 with the appropriate methods, combine split, shuffle, and join to write a function that shuffles the letters in a given string.
?> def string_shuffle(z)
?>   z.split('').shuffle.join
>> end
9. Define a hash with the keys 'one', 'two', and 'three', and the values 'uno', 'dos', and 'tres'. Iterate over the hash, and for each key/value pair print out "'#{key}' in Spanish is '#{value}'".
>> numbers = { one: 'uno', two: 'dos', three: 'tres'}
?> numbers.each do |key, value|
?>   puts "'#{key}' in Spanish is '#{value}'"
>> end
10. Create three hashes called person1, person2, and person3, with first and last names under the keys :first and :last. Then create a params hash so that params[:father] is person1, params[:mother] is person2, and params[:child] is person3. Verify that, for example, params[:father][:first] has the right value.
>> person1 = {:firstname => "Father", :lastname => "Joe"}
=> {:firstname=>"Father", :lastname=>"Joe"}
>> person2 = {:firstname => "Mother", :lastname => "Joe"}
=> {:firstname=>"Mother", :lastname=>"Joe"}
>> person3 = {:firstname => "Son", :lastname => "Joe"}
=> {:firstname=>"Son", :lastname=>"Joe"}
>> params = {:father => person1, :mother => person2, :child => person3}
=> 
{:father=>{:firstname=>"Father", :lastname=>"Joe"},
...
>> params[:father]
=> {:firstname=>"Father", :lastname=>"Joe"}
11. Define a hash with symbol keys corresponding to name, email, and a “password digest”, and values equal to your name, your email address, and a random string of 16 lower-case letters.
>> pass = ('a'..'z').to_a.shuffle[0..16].join
=> "idykemtafnopwhgvr"
>> user = { name: "John Doe", email: "jd@mail.com", :'password digest' => "#{pass}" }
=> {:name=>"John Doe", :email=>"jd@mail.com", :"password digest"=>"idykemtafnopwhgvr"}
12. Find an online version of the Ruby API and read about the Hash method merge. What is the value of the following expression?
Hash class method which can add the content the given hash array to the other. Entries with duplicate keys are overwritten with the values from each other_hash successively if no block is given.
=> {"a"=>100, "b"=>300}
### 4.4
1. What is the literal constructor for the range of integers from 1 to 10?
(1..10)
2. What is the constructor using the Range class and the new method? Hint: new takes two arguments in this context.
>> x = Range.new(1,10)
=> 1..10
3. Confirm using the == operator that the literal and named constructors from the previous two exercises are identical.
>> (1..10) == x
=> true
4. What is the class hierarchy for a range? For a hash? For a symbol?
Range, Has, Symbol - Object - BasicObject
5. Confirm that the method shown in Listing 4.15 works even if we replace self.reverse with just reverse.
?> class Word < String
?>     self == self.reverse
?>   end
>> o.palindrome?
=> true
6. Verify that “racecar” is a palindrome and “onomatopoeia” is not. What about the name of the South Indian language “Malayalam”? Hint: Downcase it first
>> o = Word.new("racecar")
=> "racecar"
>> o.palindrome?
=> true
>> o = Word.new("onomatopeia")
=> "onomatopeia"
>> o.palindrome?
=> false
?> class Word < String
?>   def palindrome?
?>     self.downcase == self.downcase.reverse
?>   end
>> end
=> :palindrome?
>> p
=> "Malayalam"
>> p.palindrome?
=> true
7. Using Listing 4.16 as a guide, add a shuffle method to the String class. Hint: Refer to Listing 4.12.
?> class String
?>   def shuffle
?>     self.split('').shuffle.join
?>   end
>> end
>> "foobar".shuffle
=> "oafbor"
### 4.5
1. By running the Rails console in the toy app’s directory from Chapter 2, confirm that you can create a user object using User.new.
>> user = User.new(name: "John Doe", email: "jd@mail.com")
   (3.0ms)  SELECT sqlite_version(*)
=> #<User id: nil, name: "John_Doe", email: "jd@mail.com", created_at: nil, updated_at: nil>
2. Determine the class hierarchy of the user object.
>> user.class.superclass
=> ApplicationRecord(abstract)
>> user.class.superclass.superclass
=> ActiveRecord::Base
>> user.class.superclass.superclass.superclass
=> Object
>> user.class.superclass.superclass.superclass.superclass
=> BasicObject
3. In the example User class, change from name to separate first and last name attributes, and then add a method called full_name that returns
the first and last names separated by a space. Use it to replace the use of name in the formatted email method.

class User
    attr_accessor :first, :last, :email, :full_name
  
    def initialize(attributes = {})
      @first  = attributes[:first]
      @last  = attributes[:last]
      @email = attributes[:email]
      @full_name = full_name
    end
  
    def formatted_email
      "#{@full_name} <#{@email}>"
    end
    
    def full_name
      "#{@first} #{@last}"
    end

end
4. . Add a method called alphabetical_name that returns the last name and first name separated by comma-space.
class User
  def alphabetical_name
    "#{@last}, #{@first}"
  end
end
5. Verify that full_name.split is the same as alphabetical_name.-split(', ').reverse.
=> true

